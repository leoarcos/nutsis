/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import DAO.users_DAO;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Leo
 */
public class users_DTO {
   
     public ResultSet logIn(String user, String pass) throws SQLException{
        
        users_DAO res= new users_DAO();
        ResultSet result= res.logIn(user, pass);
        
        return result;
    }
}
